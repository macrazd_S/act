var express = require('express');
var fs = require('fs');
var multiparty = require('multiparty');
// 腾讯云的文件存储SDK
var CosSdk = require('cos-nodejs-sdk-v5');
var router = express.Router();
var loginCheckMiddleware = require('../util').loginCheckMiddleware;
var mysql = require('../util').mysql;
var config = require('../config');
// 获取腾讯云配置
// serverHost, tunnelServerUrl, tunnelSignatureKey, qcloudAppId, qcloudSecretId, qcloudSecretKey, wxMessageToken
var qcloudConfig = JSON.parse(fs.readFileSync('/data/release/sdk.config.json', 'utf8'));
var userTable = 'user_info';
// 文件存储sdk初始化
var cos = new CosSdk({
  AppId: qcloudConfig.qcloudAppId,
  SecretId: qcloudConfig.qcloudSecretId,
  SecretKey: qcloudConfig.qcloudSecretKey
});

router.use(loginCheckMiddleware);

router.all('*', function (req, res, next) {
  if (!req.session) {
    res.status(401).json({
      error: '未登录'
    });
    return;
  }
  next();
});

// 获取用户
router.get('/', function (req, res, next) {
  console.log('Getting userInfo');
  mysql(userTable).where({
    open_id: req.session.open_id
  })
    .select('*')
    .then(function (result) {
      console.log(result);
      if (result[0]) {
        mysql('school_direction').where({
          sch_id: result[0].sch_id
        }).then(function (ret) {
          console.log(ret);
          if (ret[0]) {
            var data = {
              open_id: result[0].open_id,
              user_group_id: result[0].user_group_id,
              name: result[0].user_name,
              school_name: ret[0].sch_name,
              province: ret[0].province,
              city: ret[0].city,
              sch_id: result[0].sch_id,
              habit_1: result[0].habit_1,
              habit_2: result[0].habit_2,
              habit_3: result[0].habit_3,
              habit_4: result[0].habit_4,
              habit_5: result[0].habit_5,
              habit_6: result[0].habit_6,
              avatar: result[0].user_avatar
            }
            res.json(data);
          }
          else {
            sch_name = null;
          }
        });
      }
      else {
        res.status(400).json({
          error: '未创建用户'
        });
      }
    });
});

//新增用户
router.post('/', function (req, res, next) {
  console.log(req);
  var userInfo = req.body;
  var resUserInfo = req.body;
  var sch_id = null;
  mysql('school_direction').where({
    sch_name: userInfo.school_name
  }).then(function (ret) {
    console.log(ret);
    if (ret[0]) {
      sch_id = ret[0].sch_id;
      console.log('ok' + sch_id);
    }
  });
  mysql(userTable).where({
    open_id: req.session.open_id
  })
    //.count('open_id as hasUser')
    .then(function (ret) {
      if (ret[0]) {
        res.status(400).json({
          data: ret[0],
          error: '用户已创建'
        });
      }
      else {
        userInfo = {
          open_id: req.session.open_id,
          user_group_id: userInfo.user_group_id,
          user_name: userInfo.name,
          sch_id: sch_id,
          habit_1: userInfo.habit_1,
          habit_2: userInfo.habit_2,
          habit_3: userInfo.habit_3,
          habit_4: userInfo.habit_4,
          habit_5: userInfo.habit_5,
          habit_6: userInfo.habit_6,
          user_avatar: userInfo.avatar
        };
        console.log(userInfo);
        mysql(userTable).insert(userInfo)
          .then(function () {
            //delete userInfo.open_id;
            resUserInfo.sch_id = userInfo.sch_id;
            res.json(resUserInfo);
          });
      }
    });
});


router.post('/chgsch', function (req, res, next) {
  console.log(req);
  var id = req.body.open_id;
  mysql('school_direction')
    .where({
      province: req.body.province,
      city: req.body.city,
      sch_name: req.body.sch_name
    })
    .select('*').then(function (ret) {
      console.log(ret);
      mysql(userTable)
        .where({
          open_id: id
        })
        .update({
          sch_id: ret[0].sch_id
        }).then(function (result) {
          res.status(200).json({msg:'修改成功'});
          console.log(result);
        })
    });
});

router.post('/hobby', function (req, res, next) {
  console.log(req);
  var open_id = req.body.open_id;
  var hobby = [];
  hobby=req.body.hobby;
  console.log(hobby);
  mysql(userTable)
    .where({ open_id: open_id })
    .update({
      habit_1: hobby[0],
      habit_2: hobby[1],
      habit_3: hobby[2],
      habit_4: hobby[3],
      habit_5: hobby[4],
      habit_6: hobby[5],
    }).then(function(ret){
      res.status(200).json({ msg: '修改成功' });
      console.log(ret);
    })
});

module.exports = router;