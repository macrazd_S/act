var express = require('express');
var request = require('request');
var crypto = require('crypto');
var moment = require('moment');
var router = express.Router();
var config = require('../config');
var mysql = require('../util').mysql;
var actTable = 'activity_info';

router.get('/',function(req,res,next){
  console.log(req);
  var key='%'+req.query.key+'%';
  var id = req.query.open_id;
  console.log(key);
  mysql(actTable)
    .where('activity_name','like',key)
    .select('*')
    .orderBy('activity_id', 'asc')
    .then(function(ret){
      var tmp = [];
      var j = 0;
      var i = 0;
      var open_id = id;
      while (ret[j]) {
        ret[j] = ret[j].activity_id;
        j++;
      }
      var promises = [];
      var promises2 = [];
      var promises3 = [];
      var promises4 = [];
      //回调函数会先执行完回调函数之外的部分，再执行回调函数
      //用promise可以阻塞当前，执行回调函数，执行完回调函数再继续执行后面的代码
      for (i = 0; i < j; i++) {
        promises.push(
          mysql('activity_info')
            .where({ activity_id: ret[i] })
            .select('*')
        )
      }
      Promise.all(promises).then(result => {
        var id = [];
        for (i = 0; i < j; i++) {
          //ret[i]=result[i][0];
          ret[i] = {
            activity_id: result[i][0].activity_id,
            activity_name: result[i][0].activity_name,
            activity_range: result[i][0].activity_range,
            activity_tag1: result[i][0].activity_tag1,
            activity_tag2: result[i][0].activity_tag2,
            activity_tag3: result[i][0].activity_tag3,
            activity_tag4: result[i][0].activity_tag4,
            activity_tag5: result[i][0].activity_tag5,
            activity_tag6: result[i][0].activity_tag6,
            appendix_url: result[i][0].appendix_url,
            activity_place: result[i][0].activity_place,
            classification_id: result[i][0].classification_id,
            community_id: result[i][0].community_id,
            cover_url: result[i][0].cover_url,
            end_time: result[i][0].end_time,
            manneger_email: result[i][0].manneger_email,
            manneger_open_id: result[i][0].manneger_open_id,
            manneger_qq: result[i][0].manneger_qq,
            manneger_tel: result[i][0].manneger_tel,
            other_matter: result[i][0].other_matter,
            passage_url: result[i][0].passage_url,
            sch_id: result[i][0].sch_id,
            sch_name: null,
            sta_id: result[i][0].sta_id,
            start_time: result[i][0].start_time,
            community_name: null,
            like: false
          }
        }
        for (i = 0; i < j; i++) {
          promises2.push(
            mysql('school_direction')
              .where({ sch_id: ret[i].sch_id })
              .select('sch_name')
          )
        }
        Promise.all(promises2).then(sch_name => {
          //console.log(sch_name);
          for (i = 0; i < j; i++) {
            ret[i].sch_name = sch_name[i][0].sch_name;
          }
          for (i = 0; i < j; i++) {
            promises3.push(
              mysql('community_info')
                .where({ community_id: ret[i].community_id })
                .select('community_name')
            )
          }
          Promise.all(promises3).then(community_name => {
            //console.log(sch_name);
            for (i = 0; i < j; i++) {
              ret[i].community_name = community_name[i][0].community_name;
            }
            promises4.push(
              mysql('favorite_activity')
                .where({ open_id: open_id })
                .select('activity_id')
                .orderBy('activity_id', 'asc')
            )
            Promise.all(promises4).then(activity_id => {
              console.log(activity_id);
              var x;
              for (i = 0; i < j; i++) {
                for (x = 0; x < activity_id[0].length; x++) {
                  if (activity_id[0][x].activity_id == ret[i].activity_id) {
                    ret[i].like = true;
                    console.log(ret[i]);
                  }
                }
              }
              res.json(ret);
            })
            //res.json(ret);
          });
        });
        //ret = result;
      })
    })
});


module.exports = router;