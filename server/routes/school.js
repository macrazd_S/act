var express = require('express');
var request = require('request');
var crypto = require('crypto');
var moment = require('moment');
var router = express.Router();
var config = require('../config');
var mysql = require('../util').mysql;
var schoolTable='school_direction';

router.get('/',function(req,res,next){
  var pro = req.query.province;
  var city = req.query.city;
  mysql(schoolTable)
    .where({
      province: pro,
      city:city
    }).then(function(ret){
      var i=0;
      while(ret[i]){
        ret[i]=ret[i].sch_name;
        i++;
      }
      console.log(ret);
      res.json(ret);
    })
});

router.get('/province',function(req,res,next){
  console.log(req);
  mysql(schoolTable)
    .distinct('province')
    .select().then(function(ret){
      var i=0;
      while(ret[i]){
        ret[i]=ret[i].province;
        i++;
      }
      res.json(ret);
    })
});

router.get('/city',function(req,res,next){
  var pro=req.query.province;
  mysql(schoolTable)
    .where({
      province: pro
    })
    .distinct('city')
    .select()
    .then(function(ret){
      var i=0;
      while (ret[i]) {
        ret[i] = ret[i].city;
        i++;
      }
      res.json(ret);
    })
});
module.exports = router;