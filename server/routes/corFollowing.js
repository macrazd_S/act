var express = require('express');
var request = require('request');
var crypto = require('crypto');
var moment = require('moment');
var router = express.Router();
var config = require('../config');
var mysql = require('../util').mysql;
var corFollowingTable = 'follow_community';

router.get('/', function (req, res, next) {
  console.log(req);
  var open_id = req.query.open_id;
  var corFolInfo = [];
  mysql(corFollowingTable)
    .where({ open_id: open_id })
    .select().then(function (ret) {
      if(ret.length==0){
        res.json({});
        return;
      }
      var num = 0;
      var i = 0;
      var j = 0;
      var k = 0;
      var l = 0;
      var n = 0;
      var promise = [];
      var open_id;
      var sch_id;
      var community_id;
      for (i = 0; i < ret.length; i++) {
        var p1 = [];
        promise[i] = p1;
        console.log('out', promise);
        promise[i].push(
          mysql('community_info')
            .where({ community_id: ret[i].community_id })
            .select('*')
        );
        console.log(k);
        Promise.all(promise[k++]).then(result => {
          console.log('p1', j, result, k);
          var m = -1;
          var o = -1;
          community_id = result[0][0].community_id;
          open_id = result[0][0].president_open_id;
          sch_id = result[0][0].sch_id;
          var name = result[0][0].community_name;
          var avatar = result[0][0].community_avatar;
          corFolInfo[j] = {
            open_id: open_id,
            community_id: community_id,
            community_name: name,
            community_avatar: avatar,
            pro_name: null,
            sch_name: null
          };
          var p2 = [];
          promise[k + corFolInfo.length] = p2;
          promise[k + corFolInfo.length].push(
            mysql('user_info')
              .where({ open_id: open_id })
              .select('*')
          );
          j++;
          Promise.all(promise[k + corFolInfo.length]).then(r2 => {
            console.log('p2', l, r2, k);
            corFolInfo[l].pro_name = r2[0][0].user_name;
            var p3 = [];
            promise[k + corFolInfo.length] = p3;
            promise[k + corFolInfo.length].push(
              mysql('school_direction')
                .where({ sch_id: sch_id })
                .select('*')
            );
            console.log(corFolInfo);
            l = l + 1;
            Promise.all(promise[k + corFolInfo.length]).then(r3 => {
              console.log('p3', n, k);
              corFolInfo[n].sch_name = r3[0][0].sch_name;
              console.log(corFolInfo);
              n++;
              if (n == corFolInfo.length) return corFolInfo;
              else return 0;
            }).then(r => {
              if (r == 0) return;
              console.log(r);
              res.json(r);
            })
            k++;
          });
          k++;
        });
      }
    })
});
router.get('/corList', function (req, res, next) {
  var sch_id = req.query.sch_id;
  var open_id = req.query.open_id;
  var promise = [];
  var i,j;
  var k=0;
  var flag=false;
  var tmp=[];
  mysql('community_info').where({ sch_id: sch_id })
    .select('*').then(function (ret) {
      console.log(ret);
      tmp=ret;
      for (i = 0; i < ret.length; i++) {
        promise.push(
          mysql(corFollowingTable).where({open_id:open_id,community_id:ret[i].community_id}).select('*')
        )
      }
      Promise.all(promise).then(r=>{
        console.log(r);
        var result=[];
        for(i=0;i<tmp.length;i++){
          if(r[i].length==0){
            result[k] = tmp[i];
            k++;
            continue;
          }
        }
        res.json(result);
      })
    })
});
router.post('/followCor', function (req, res, next) {
  mysql(corFollowingTable).insert(req.body)
    .then(function (ret) {
      res.json(ret);
    })
});
router.delete('/', function (req, res, next) {
  console.log(req);
  var corId = req.body.corDat.community_id;
  var open_id = req.body.open_id;
  mysql(corFollowingTable)
    .where({ open_id: open_id, community_id: corId })
    .select('*').del().then(function (ret) {
      res.json(ret);
    })
});
module.exports = router;