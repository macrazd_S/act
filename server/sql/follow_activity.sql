-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:48
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `follow_activity`
--

CREATE TABLE `follow_activity` (
  `f_open_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'open id',
  `f_activity_id_1` int(11) DEFAULT NULL COMMENT '社团1id',
  `f_activity_id_2` int(11) DEFAULT NULL COMMENT '社团2id',
  `f_activity_id_3` int(11) DEFAULT NULL COMMENT '社团3id',
  `f_activity_id_4` int(11) DEFAULT NULL COMMENT '社团4id',
  `f_activity_id_5` int(11) DEFAULT NULL COMMENT '社团5id',
  `f_activity_id_6` int(11) DEFAULT NULL COMMENT '社团6id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='关注社团表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `follow_activity`
--
ALTER TABLE `follow_activity`
  ADD KEY `f_activity_id_1` (`f_activity_id_1`) USING BTREE,
  ADD KEY `f_activity_id_2` (`f_activity_id_2`) USING BTREE,
  ADD KEY `f_activity_id_3` (`f_activity_id_3`) USING BTREE,
  ADD KEY `f_activity_id_4` (`f_activity_id_4`) USING BTREE,
  ADD KEY `f_activity_id_5` (`f_activity_id_5`) USING BTREE,
  ADD KEY `f_activity_id_6` (`f_activity_id_6`) USING BTREE;

--
-- 限制导出的表
--

--
-- 限制表 `follow_activity`
--
ALTER TABLE `follow_activity`
  ADD CONSTRAINT `follow_activity_ibfk_1` FOREIGN KEY (`f_activity_id_1`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `follow_activity_ibfk_2` FOREIGN KEY (`f_activity_id_2`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `follow_activity_ibfk_3` FOREIGN KEY (`f_activity_id_3`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `follow_activity_ibfk_4` FOREIGN KEY (`f_activity_id_4`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `follow_activity_ibfk_5` FOREIGN KEY (`f_activity_id_5`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `follow_activity_ibfk_6` FOREIGN KEY (`f_activity_id_6`) REFERENCES `community_info` (`community_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
