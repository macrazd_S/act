-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:53
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `school_info`
--

CREATE TABLE `school_info` (
  `sch_id` int(11) NOT NULL COMMENT '学校id',
  `sch_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '学校名',
  `f_p_id` int(11) NOT NULL COMMENT '省id',
  `f_c_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '市名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='学校信息表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `school_info`
--
ALTER TABLE `school_info`
  ADD PRIMARY KEY (`sch_name`) USING BTREE,
  ADD KEY `f_p_id` (`f_p_id`) USING BTREE,
  ADD KEY `f_c_name` (`f_c_name`) USING BTREE,
  ADD KEY `sch_id` (`sch_id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `school_info`
--
ALTER TABLE `school_info`
  MODIFY `sch_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '学校id';
--
-- 限制导出的表
--

--
-- 限制表 `school_info`
--
ALTER TABLE `school_info`
  ADD CONSTRAINT `school_info_ibfk_1` FOREIGN KEY (`f_p_id`) REFERENCES `provincial` (`p_id`),
  ADD CONSTRAINT `school_info_ibfk_2` FOREIGN KEY (`f_c_name`) REFERENCES `city` (`c_name`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
