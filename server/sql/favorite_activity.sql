-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:44
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `favorite_activity`
--

CREATE TABLE `favorite_activity` (
  `f_open_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户open id',
  `f_activity_id` int(11) DEFAULT NULL COMMENT '活动id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='收藏活动表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `favorite_activity`
--
ALTER TABLE `favorite_activity`
  ADD KEY `f_open_id` (`f_open_id`) USING BTREE,
  ADD KEY `f_activity_id` (`f_activity_id`) USING BTREE;

--
-- 限制导出的表
--

--
-- 限制表 `favorite_activity`
--
ALTER TABLE `favorite_activity`
  ADD CONSTRAINT `favorite_activity_ibfk_1` FOREIGN KEY (`f_open_id`) REFERENCES `session` (`open_id`),
  ADD CONSTRAINT `favorite_activity_ibfk_2` FOREIGN KEY (`f_activity_id`) REFERENCES `activity_info` (`activity_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
