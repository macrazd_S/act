-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:21
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `activity_position_check`
--

CREATE TABLE `activity_position_check` (
  `check_id` int(11) NOT NULL COMMENT '审核id',
  `f_open_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'opne id',
  `f_activity_id` int(11) DEFAULT NULL COMMENT '社团id',
  `f_community_position_id` int(11) DEFAULT NULL COMMENT '社团职位id',
  `f_sch_id` int(11) DEFAULT NULL COMMENT '学校id',
  `user_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户名',
  `user_grade` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '用户年级',
  `check_status` int(11) DEFAULT NULL COMMENT '审核状态',
  `check_message` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审核留言',
  `apply_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '申请时间',
  `check_answer` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '审核回复'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='社团职位审核表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_position_check`
--
ALTER TABLE `activity_position_check`
  ADD PRIMARY KEY (`check_id`) USING BTREE,
  ADD KEY `f_open_id` (`f_open_id`) USING BTREE,
  ADD KEY `f_activity_id` (`f_activity_id`) USING BTREE,
  ADD KEY `f_community_position_id` (`f_community_position_id`) USING BTREE,
  ADD KEY `f_sch_id` (`f_sch_id`) USING BTREE,
  ADD KEY `check_id` (`check_id`) USING BTREE;

--
-- 限制导出的表
--

--
-- 限制表 `activity_position_check`
--
ALTER TABLE `activity_position_check`
  ADD CONSTRAINT `activity_position_check_ibfk_1` FOREIGN KEY (`f_open_id`) REFERENCES `session` (`open_id`),
  ADD CONSTRAINT `activity_position_check_ibfk_2` FOREIGN KEY (`f_activity_id`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `activity_position_check_ibfk_3` FOREIGN KEY (`f_community_position_id`) REFERENCES `community_position` (`pos_id`),
  ADD CONSTRAINT `activity_position_check_ibfk_4` FOREIGN KEY (`f_sch_id`) REFERENCES `school_info` (`sch_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
