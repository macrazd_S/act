-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:50
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `provincial`
--

CREATE TABLE `provincial` (
  `p_id` int(11) NOT NULL COMMENT '省id',
  `p_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '省名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='省信息表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `provincial`
--

INSERT INTO `provincial` (`p_id`, `p_name`) VALUES
(1, '北京市'),
(2, '天津市'),
(3, '上海市'),
(4, '重庆市'),
(5, '河北省'),
(6, '山西省'),
(7, '台湾省'),
(8, '辽宁省'),
(9, '吉林省'),
(10, '黑龙江省'),
(11, '江苏省'),
(12, '浙江省'),
(13, '安徽省'),
(14, '福建省'),
(15, '江西省'),
(16, '山东省'),
(17, '河南省'),
(18, '湖北省'),
(19, '湖南省'),
(20, '广东省'),
(21, '甘肃省'),
(22, '四川省'),
(23, '贵州省'),
(24, '海南省'),
(25, '云南省'),
(26, '青海省'),
(27, '陕西省'),
(28, '广西壮族自治区'),
(29, '西藏自治区'),
(30, '宁夏回族自治区'),
(31, '新疆维吾尔自治区'),
(32, '内蒙古自治区'),
(33, '澳门特别行政区'),
(34, '香港特别行政区');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `provincial`
--
ALTER TABLE `provincial`
  ADD PRIMARY KEY (`p_id`) USING BTREE,
  ADD KEY `p_id` (`p_id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `provincial`
--
ALTER TABLE `provincial`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '省id', AUTO_INCREMENT=35;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
