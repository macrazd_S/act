-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:32:06
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `user_info`
--

CREATE TABLE `user_info` (
  `f_open_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'open_id',
  `f_user_group_id` int(11) NOT NULL COMMENT '用户组id',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_sch_id` int(11) NOT NULL COMMENT '学校id',
  `habit_1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣1',
  `habit_2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣2',
  `habit_3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣3',
  `habit_4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣4',
  `habit_5` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣5',
  `habit_6` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '兴趣6',
  `avatar` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户信息表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD KEY `f_open_id` (`f_open_id`) USING BTREE,
  ADD KEY `f_user_group_id` (`f_user_group_id`) USING BTREE,
  ADD KEY `f_sch_id` (`f_sch_id`) USING BTREE;

--
-- 限制导出的表
--

--
-- 限制表 `user_info`
--
ALTER TABLE `user_info`
  ADD CONSTRAINT `user_info_ibfk_1` FOREIGN KEY (`f_open_id`) REFERENCES `session` (`open_id`),
  ADD CONSTRAINT `user_info_ibfk_2` FOREIGN KEY (`f_user_group_id`) REFERENCES `user_identify` (`user_group_id`),
  ADD CONSTRAINT `user_info_ibfk_3` FOREIGN KEY (`f_sch_id`) REFERENCES `school_info` (`sch_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
