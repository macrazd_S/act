-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:03
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `activity_classification`
--

CREATE TABLE `activity_classification` (
  `classification_id` int(11) NOT NULL COMMENT '分类id',
  `classification_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='活动分类表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_classification`
--
ALTER TABLE `activity_classification`
  ADD PRIMARY KEY (`classification_id`,`classification_name`) USING BTREE,
  ADD KEY `classification_id` (`classification_id`) USING BTREE,
  ADD KEY `classification_name` (`classification_name`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `activity_classification`
--
ALTER TABLE `activity_classification`
  MODIFY `classification_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类id';COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
