-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:34
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `community_info`
--

CREATE TABLE `community_info` (
  `community_id` int(11) NOT NULL COMMENT '社团id',
  `community_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '社团名',
  `f_president_open_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '会长open id',
  `f_sch_id` int(11) DEFAULT NULL COMMENT '学校id',
  `tag_1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '社团标签1',
  `tag_2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '社团标签2',
  `tag_3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '社团标签3',
  `tag_4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '社团标签4',
  `tag_5` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '社团标签5',
  `tag_6` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '社团标签6'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='社团信息表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `community_info`
--
ALTER TABLE `community_info`
  ADD PRIMARY KEY (`community_id`,`community_name`) USING BTREE,
  ADD KEY `f_president_open_id` (`f_president_open_id`) USING BTREE,
  ADD KEY `f_sch_id` (`f_sch_id`) USING BTREE,
  ADD KEY `community_id` (`community_id`) USING BTREE,
  ADD KEY `community_name` (`community_name`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `community_info`
--
ALTER TABLE `community_info`
  MODIFY `community_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '社团id';
--
-- 限制导出的表
--

--
-- 限制表 `community_info`
--
ALTER TABLE `community_info`
  ADD CONSTRAINT `community_info_ibfk_1` FOREIGN KEY (`f_president_open_id`) REFERENCES `session` (`open_id`),
  ADD CONSTRAINT `community_info_ibfk_2` FOREIGN KEY (`f_sch_id`) REFERENCES `school_info` (`sch_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
