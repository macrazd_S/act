-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:14
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `activity_info`
--

CREATE TABLE `activity_info` (
  `activity_id` int(11) NOT NULL COMMENT '活动id',
  `activity_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '活动名称',
  `f_community_id` int(11) DEFAULT NULL COMMENT '社团id',
  `f_manneger_open_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '负责人open id',
  `f_classification_id` int(11) DEFAULT NULL COMMENT '分类id',
  `f_sta_id` int(11) DEFAULT NULL COMMENT '状态id',
  `manneger_qq` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '负责人qq',
  `manneger_tel` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '负责人电话',
  `manneger_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '负责人邮箱',
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '开始时间',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '结束时间',
  `avtivity_place` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动地点',
  `activity_range` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动范围',
  `cover_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '封面URL',
  `passage_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '公众号文章URL',
  `other_matter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '其他注意事项',
  `appendix_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '附件URL',
  `activity_tag1` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标签1',
  `activity_tag2` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标签2',
  `activity_tag3` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标签3',
  `activity_tag4` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标签4',
  `activity_tag5` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标签5',
  `activity_tag6` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '活动标签6'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='活动信息表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_info`
--
ALTER TABLE `activity_info`
  ADD PRIMARY KEY (`activity_id`,`activity_name`) USING BTREE,
  ADD KEY `f_community_id` (`f_community_id`) USING BTREE,
  ADD KEY `f_manneger_open_id` (`f_manneger_open_id`) USING BTREE,
  ADD KEY `f_classification_id` (`f_classification_id`) USING BTREE,
  ADD KEY `f_sta_id` (`f_sta_id`) USING BTREE,
  ADD KEY `activity_id` (`activity_id`) USING BTREE,
  ADD KEY `activity_name` (`activity_name`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `activity_info`
--
ALTER TABLE `activity_info`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '活动id';
--
-- 限制导出的表
--

--
-- 限制表 `activity_info`
--
ALTER TABLE `activity_info`
  ADD CONSTRAINT `activity_info_ibfk_1` FOREIGN KEY (`f_community_id`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `activity_info_ibfk_2` FOREIGN KEY (`f_manneger_open_id`) REFERENCES `session` (`open_id`),
  ADD CONSTRAINT `activity_info_ibfk_3` FOREIGN KEY (`f_classification_id`) REFERENCES `activity_classification` (`classification_id`),
  ADD CONSTRAINT `activity_info_ibfk_4` FOREIGN KEY (`f_sta_id`) REFERENCES `activity_status` (`sta_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
