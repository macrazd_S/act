-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:30
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `city`
--

CREATE TABLE `city` (
  `c_id` int(11) NOT NULL COMMENT '市id',
  `c_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '市名',
  `f_p_id` int(11) NOT NULL COMMENT '省id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='市信息表' ROW_FORMAT=DYNAMIC;

--
-- 转存表中的数据 `city`
--

INSERT INTO `city` (`c_id`, `c_name`, `f_p_id`) VALUES
(10, '七台河市', 10),
(1, '上海市', 3),
(10, '临汾市', 6),
(6, '丹东市', 8),
(11, '丽水市', 12),
(16, '云林县', 7),
(15, '亳州市', 13),
(7, '伊 春 市', 10),
(9, '佳木斯市', 10),
(6, '保定市', 5),
(14, '六安市', 13),
(1, '北京市', 1),
(1, '南京市', 11),
(15, '南投县', 7),
(6, '南通市', 11),
(4, '双鸭山市', 10),
(22, '台东县', 7),
(13, '台中县', 7),
(4, '台中市', 7),
(8, '台北县', 7),
(1, '台北市', 7),
(18, '台南县', 7),
(5, '台南市', 7),
(10, '台州市', 12),
(1, '合肥市', 13),
(2, '吉林市', 9),
(11, '吕梁市', 6),
(1, '哈尔滨市', 10),
(2, '唐山市', 5),
(17, '嘉义县', 7),
(7, '嘉义市', 7),
(4, '嘉兴市', 12),
(3, '四平市', 9),
(3, '基隆市', 7),
(6, '大 庆 市', 10),
(13, '大兴安岭地区', 10),
(2, '大同市', 6),
(2, '大连市', 8),
(1, '天津市', 2),
(1, '太原市', 6),
(2, '宁波市', 12),
(8, '安庆市', 13),
(9, '宜兰县', 7),
(17, '宣城市', 13),
(12, '宿州市', 13),
(13, '宿迁市', 11),
(20, '屏东县', 7),
(13, '巢湖市', 13),
(4, '常州市', 11),
(10, '廊坊市', 5),
(9, '延边朝鲜族自治州', 9),
(7, '张家口市', 5),
(14, '彰化县', 7),
(3, '徐州市', 11),
(9, '忻州市', 6),
(10, '扬州市', 11),
(8, '承德市', 5),
(4, '抚顺市', 8),
(11, '新竹县', 7),
(6, '新竹市', 7),
(2, '无锡市', 11),
(7, '晋中市', 6),
(5, '晋城市', 6),
(6, '朔州市', 6),
(13, '朝阳市', 8),
(5, '本溪市', 8),
(1, '杭州市', 12),
(7, '松原市', 9),
(10, '桃园县', 7),
(16, '池州市', 13),
(1, '沈阳市', 8),
(9, '沧州市', 5),
(12, '泰州市', 11),
(6, '淮北市', 13),
(4, '淮南市', 13),
(8, '淮安市', 11),
(3, '温州市', 12),
(5, '湖州市', 12),
(10, '滁州市', 13),
(21, '澎湖县', 7),
(8, '牡丹江市', 10),
(8, '白城市', 9),
(6, '白山市', 9),
(9, '盐城市', 11),
(11, '盘锦市', 8),
(1, '石家庄市', 5),
(3, '秦皇岛市', 5),
(6, '绍兴市', 12),
(12, '绥 化 市', 10),
(9, '舟山市', 12),
(2, '芜湖市', 13),
(23, '花莲县', 7),
(5, '苏州市', 11),
(12, '苗栗县', 7),
(8, '营口市', 8),
(14, '葫芦岛市', 8),
(3, '蚌埠市', 13),
(11, '衡水市', 5),
(8, '衢州市', 12),
(4, '辽源市', 9),
(10, '辽阳市', 8),
(8, '运城市', 6),
(7, '连云港市', 11),
(5, '通化市', 9),
(5, '邢台市', 5),
(4, '邯郸市', 5),
(1, '重庆市', 4),
(7, '金华市', 12),
(12, '铁岭市', 8),
(7, '铜陵市', 13),
(7, '锦州市', 8),
(11, '镇江市', 11),
(1, '长春市', 9),
(4, '长治市', 6),
(9, '阜新市', 8),
(11, '阜阳市', 13),
(3, '阳泉市', 6),
(3, '鞍山市', 8),
(5, '马鞍山市', 13),
(19, '高雄县', 7),
(2, '高雄市', 7),
(5, '鸡 西 市', 10),
(3, '鹤 岗 市', 10),
(9, '黄山市', 13),
(11, '黑 河 市', 10),
(2, '齐齐哈尔市', 10);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`c_name`) USING BTREE,
  ADD KEY `f_p_id` (`f_p_id`) USING BTREE,
  ADD KEY `c_id` (`c_id`) USING BTREE;

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `city`
--
ALTER TABLE `city`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '市id', AUTO_INCREMENT=24;
--
-- 限制导出的表
--

--
-- 限制表 `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`f_p_id`) REFERENCES `provincial` (`p_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
