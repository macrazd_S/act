-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2018-05-07 12:31:38
-- 服务器版本： 5.7.18
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `actTest`
--

-- --------------------------------------------------------

--
-- 表的结构 `community_manneger`
--

CREATE TABLE `community_manneger` (
  `f_community_id` int(11) DEFAULT NULL COMMENT '社团id',
  `f_manneger_open_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '管理员open id',
  `f_pos_id` int(11) DEFAULT NULL COMMENT '职位id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='社团管理员表' ROW_FORMAT=DYNAMIC;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `community_manneger`
--
ALTER TABLE `community_manneger`
  ADD KEY `f_community_id` (`f_community_id`) USING BTREE,
  ADD KEY `f_manneger_open_id` (`f_manneger_open_id`) USING BTREE,
  ADD KEY `f_pos_id` (`f_pos_id`) USING BTREE;

--
-- 限制导出的表
--

--
-- 限制表 `community_manneger`
--
ALTER TABLE `community_manneger`
  ADD CONSTRAINT `community_manneger_ibfk_1` FOREIGN KEY (`f_community_id`) REFERENCES `community_info` (`community_id`),
  ADD CONSTRAINT `community_manneger_ibfk_2` FOREIGN KEY (`f_manneger_open_id`) REFERENCES `session` (`open_id`),
  ADD CONSTRAINT `community_manneger_ibfk_3` FOREIGN KEY (`f_pos_id`) REFERENCES `community_position` (`pos_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
