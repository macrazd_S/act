var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config');
var login = require('./routes/login');
var user = require('./routes/user');
var school = require('./routes/school');
var actList = require('./routes/actList');
var actFollowing = require('./routes/actFollowing');
var corFollowing = require('./routes/corFollowing');
var search = require('./routes/search');
var recommendAct=require('./routes/recommendAct');
var corAdmin=require('./routes/corAdmin');
var app = express();
var port = config.port;
app.use(bodyParser.json());
app.get('/', function (req, res) {
  res.send("Welcome to Act-cov");
});
app.use('/login', login);
app.use('/user', user);
app.use('/school', school);
app.use('/actList', actList);
app.use('/actFollowing', actFollowing);
app.use('/corFollowing', corFollowing);
app.use('/recommendAct',recommendAct);
app.use('/search', search);
app.use('/corAdmin',corAdmin);
app.use(function (req, res, next) {
  res.status(404).json({
    error: '资源未找到'
  });
});
/*
这个的next()表示
   |
   V
*/
app.use(function (error, req, res, next) {
  console.log(error);
  res.status(500).json({
    error: '服务器内部错误'
  });
});
/*
这个的next()表示
   |
   V
*/
app.listen(port, function (error) {
  if (error) {
    console.log('error!');
  }
  else {
    console.log("Server start! Listening on localhost:" + port);
  }
});