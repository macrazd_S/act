module.exports = {
  port: '5757',
  //过期时间，秒
  expireTime: 24 * 3600,
  appId: '',
  secret: '',
  mysql: {
    host: 'localhost',
    port: 3306,
    user: 'root',
    db: 'actTest',
    pass: '',
    char: 'utf8mb4'
  },
  //文件云存储
  coverCos: {
    region: 'ap-guangzhou',
    // Bucket 名称
    fileBucket: 'cover',
    // 文件夹
    uploadFolder: ''
  },
  themeCos: {
    region: 'ap-guangzhou',
    // Bucket 名称
    fileBucket: 'theme',
    // 文件夹
    uploadFolder: ''
  },
  otherCos: {
    region: 'ap-guangzhou',
    // Bucket 名称
    fileBucket: 'other',
    // 文件夹
    uploadFolder: ''
  },
};