var config = require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actFollowing: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    var that = this;
    console.log(getApp().globalData.userInfo.open_id);
    wx.request({
      url: config.host + '/actFollowing',
      data: { open_id: getApp().globalData.userInfo.open_id },
      success: function (res) {
        that.setData({ actFollowing: res.data });
        console.log(that.data.actFollowing);
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  unCollect: function (result) {
    console.log(result);
    var that = this;
    wx.request({
      url: config.host + '/actFollowing',
      method: 'DELETE',
      data: {
        open_id: getApp().globalData.userInfo.open_id,
        activity_id: result.currentTarget.id,
      },
    });
    //that.data.actFollowing[result.currentTarget.dataset.id] = null;
    /*
    var i = 0;
    var change = [];
    for (i = 0; i < that.data.actFollowing.length-1; i++) {
      //that.data.actFollowing[i] = that.data.actFollowing[i+1];
      if (i < result.currentTarget.dataset.id) {
        change[i] = that.data.actFollowing[i];
      } else if (i == result.currentTarget.dataset.id){
        continue;
      }else{
        change[i - 1] = that.data.actFollowing[i];
      }
    }
    this.data.actFollowing=change;
    */
    this.onLoad();
  }
})