var config=require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actList: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    /*
    var that=this;
    wx.request({
      url: config.host+'/corFollowing/corList',
      data: {
        sch_id: getApp().globalData.userInfo.sch_id,
        open_id: getApp().globalData.userInfo.open_id
        },
      success:function(res){
        console.log(res);
        that.setData({actList:res.data})
      }
    })*/
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.request({
      url: config.host + '/corFollowing/corList',
      data: {
        sch_id: getApp().globalData.userInfo.sch_id,
        open_id: getApp().globalData.userInfo.open_id
      },
      success: function (res) {
        console.log(res);
        that.setData({ actList: res.data })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  follow:function(e){
    console.log(e);
    var id=e.target.id;
    var that=this;
    wx.request({
      url: config.host+'/corFollowing/followCor',
      method:'POST',
      data:{
        community_id:that.data.actList[id].community_id,
        open_id:getApp().globalData.userInfo.open_id
        },
      success:function(res){
        that.onShow();
        console.log(res);
      }
    });
  }
})