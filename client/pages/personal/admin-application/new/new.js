var config=require('../../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cor_array: [],
    cor_index: 0,
    lv_array: ['部长', '副会长'],
    lv_index: 0,
    corInfoList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var result = [];
    wx.request({
      url: config.host + '/corAdmin',
      data: { open_id: getApp().globalData.userInfo.open_id },
      success: function (res) {
        console.log(res.data);
        var i;
        for (i = 0; i < res.data.length; i++) {
          result[i] = res.data[i].community_name;
        }
        that.setData({
          corInfoList: res.data,
          cor_array: result
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindRegionChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value
    })
  },
  
  bindPickerChange1: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      sch_index: e.detail.value
    })
  },  
  bindPickerChange2: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      cor_index: e.detail.value
    })
  }, 
  bindPickerChange3: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      grade_index: e.detail.value
    })
  }, 
  bindPickerChange4: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      lv_index: e.detail.value
    })
  }, 
  formSubmit:function(e){
    console.log(e);
    var that=this;
    var corId=e.detail.value.cor_range;
    var posId=e.detail.value.lv_range;
    var appInfo={
      open_id:getApp().globalData.userInfo.open_id,
      position:that.data.lv_array[posId],
      community_id:that.data.corInfoList[corId].community_id
    }
    wx.request({
      url: config.host+'/corAdmin/newAdminApp',
      method:'POST',
      data:appInfo,
      success:function(res){
        console.log(res);
      }
    })
  }
})