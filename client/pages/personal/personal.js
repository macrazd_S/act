var config = require('../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    avatar: '',
    //选择学校相关
    sch_set: true,
    schArr: [],
    arrIndex: 0,
    provincial: null,
    provinceItem: [],
    proIndex: 0,
    city: null,
    cityItem: [],
    cityIndex: 0,
    sch_chs: null,
    sch_chs_city: null,
    sch_chs_pro: null,
    hobby_item: [],
    hobby_chs: [],
    hobby1_Index: 0,
    hobby2_Index: 0,
    hobby3_Index: 0,
    hobby4_Index: 0,
    hobby5_Index: 0,
    hobby6_Index: 0,
    userHobby: [],
    hobby_set: true
  },

  /**
   * 生命周期函数--监听页面加载
   */

  onLoad: function (options) {
    var i = 0;
    var hbi = ['无'];
    var that = this;
    for (i = 1; i < 13; i++) {
      hbi[i] = getApp().globalData.hobby[i - 1];
    }
    //获取用户姓名、头像、兴趣列表
    this.setData({
      name: getApp().globalData.userInfo.name,
      avatar: getApp().globalData.userInfo.avatar,
      hobby_item: hbi
    });
    //获取省、市
    this.setData({ provinceItem: getApp().globalData.province });
    i = 0;
    while (getApp().globalData.userInfo.province != this.data.provinceItem[i]) {
      i++;
    }
    this.setData({
      sch_chs_pro: this.data.provinceItem[i],
      proIndex: i
    })
    var that = this;
    getApp().reqCity({
      data: {
        province: that.data.provinceItem[that.data.proIndex]
      },
      success: function (res) {
        that.setData({ cityItem: res.data });
        i = 0;
        while (getApp().globalData.userInfo.city != that.data.cityItem[i]) {
          i++;
        }
        that.setData({
          sch_chs_city: that.data.cityItem[i],
          cityIndex: i
        })
        var th = that;
        getApp().reqSch({
          data: {
            province: th.data.sch_chs_pro,
            city: th.data.sch_chs_city
          },
          success: function (ret) {
            th.setData({ schArr: ret.data });
            i = 0;
            while (getApp().globalData.userInfo.city != th.data.cityItem[i]) {
              i++;
            }
            th.setData({
              sch_chs: th.data.schArr[i],
              arrIndex: i
            })
          }
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '获取市列表失败',
          duration: 3000
        })
      }
    });
    //同步用户兴趣
    var hb = [];
    hb[0] = getApp().globalData.userInfo.habit_1;
    hb[1] = getApp().globalData.userInfo.habit_2;
    hb[2] = getApp().globalData.userInfo.habit_3;
    hb[3] = getApp().globalData.userInfo.habit_4;
    hb[4] = getApp().globalData.userInfo.habit_5;
    hb[5] = getApp().globalData.userInfo.habit_6;
    this.setData({ userHobby: hb });
    i = 0;
    while (i < 13 && this.data.userHobby[0] && this.data.userHobby[0] != this.data.hobby_item[i]) i++;
    var a = 0;
    while (a < 13 && this.data.userHobby[1] != null && this.data.userHobby[1] != this.data.hobby_item[a]) a++;
    var b = 0;
    while (b < 13 && this.data.userHobby[2] != null && this.data.userHobby[2] != this.data.hobby_item[b]) b++;
    var c = 0;
    while (c < 13 && this.data.userHobby[3] != null && this.data.userHobby[3] != this.data.hobby_item[c]) c++;
    var d = 0;
    while (d < 13 && this.data.userHobby[4] != null && this.data.userHobby[4] != this.data.hobby_item[d]) d++;
    var e = 0;
    while (e < 13 && this.data.userHobby[5] != null && this.data.userHobby[5] != this.data.hobby_item[e]) e++;
    this.setData({
      hobby_chs: [this.data.hobby_item[i], this.data.hobby_item[a], this.data.hobby_item[b], this.data.hobby_item[c], this.data.hobby_item[d], this.data.hobby_item[e]],
      hobby1_Index: i,
      hobby2_Index: a,
      hobby3_Index: b,
      hobby4_Index: c,
      hobby5_Index: d,
      hobby6_Index: e
    });
    console.log(this.data);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  schSet: function () {
    /*
    wx.navigateTo({
      url: 'school-setting/school-setting',
    })
    */
    this.setData({
      sch_set: !this.data.sch_set
    });
  },

  college: function () {
    wx.navigateTo({
      url: 'collection/collection',
    })
  },
  hobby: function () {

    this.setData({
      hobby_set: !this.data.hobby_set
    });
  },
  setting: function () {
    wx.navigateTo({
      url: 'program-setting/program-setting',
    })
  },
  aboutUs: function () {
    wx.navigateTo({
      url: 'program-setting/aboutUs/aboutUs',
    })
  },
  adminaApplication: function () {
    wx.navigateTo({
      url: 'admin-application/admin-application',
    })
  },
  reminding: function () {
    wx.navigateTo({
      url: 'reminding/reminding',
    })
  },
  corporationFollowing: function () {
    wx.navigateTo({
      url: 'corporation-following/corporation-following',
    })
  },

  chsProvince: function (e) {
    if (e.detail.value == this.data.proIndex) {
      return;
    }
    var that = this;
    this.setData({
      proIndex: e.detail.value,
    });
    this.data.sch_chs_pro= this.data.provinceItem[this.data.proIndex]
    this.setData({
      provincial: this.data.sch_chs_pro,
    });
    //console.log(this.data.sch_chs_pro);
    getApp().reqCity({
      data: {
        province: that.data.provincial
      },
      success: function (res) {
        console.log(res);
        that.setData({ cityItem: res.data });
        that.data.city = that.data.cityItem[0];
        var th = that;
        getApp().reqSch({
          data: {
            province: th.data.provincial,
            city: th.data.city
          },
          success: function (ret) {
            th.setData({ schArr: ret.data });
            th.data.school = th.data.schArr[0];
          }
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '获取市列表失败',
          duration: 3000
        })
      }
    });
  },
  chsCity: function (e) {
    if (e.detail.value == this.data.cityIndex) {
      return;
    }
    var that = this;
    this.setData({
      cityIndex: e.detail.value,
    });
    this.data.sch_chs_city = this.data.cityItem[this.data.cityIndex];
    this.setData({
      city: this.data.sch_chs_city,
    });
    getApp().reqSch({
      data: {
        province: that.data.provincial,
        city: that.data.city
      },
      success: function (ret) {
        console.log(ret);
        that.setData({ schArr: ret.data });
        that.data.school = that.data.schArr[0];
      },
      fail: function (res) {
        wx.showToast({
          title: '获取学校列表失败',
          duration: 3000
        })
      }
    });
  },
  chsSchool: function (e) {
    if (e.detail.value == this.data.arrIndex) {
      return;
    }
    this.setData({
      arrIndex: e.detail.value,
    });
    this.data.sch_chs = this.data.schArr[this.data.arrIndex];
  },
  chgSch: function (res) {
    var that = this;
    console.log(this.data.sch_chs);
    getApp().request({
      url: '/user/chgsch',
      method: 'POST',
      data: {
        open_id: getApp().globalData.userInfo.open_id,
        province: that.data.sch_chs_pro,
        city: that.data.sch_chs_city,
        sch_name: that.data.sch_chs
      },
      success: function (res) {
        if (res.statusCode == 200) {
          getApp().globalData.userInfo.school_name = that.data.sch_chs;
          wx.showToast({
            title: '修改成功',
            duration: 1500
          });
        } else {
          wx.showToast({
            icon: 'none',
            title: '修改失败',
            duration: 1500
          });
        }
      }
    })
    this.setData({
      sch_set: !this.data.sch_set
    });
  },
  closeModel: function () {
    this.setData({
      sch_set: !this.data.sch_set
    });
  },

  chsHby1: function (e) {
    if (e.detail.value == this.data.hobby1_Index) {
      return;
    }
    this.setData({
      hobby1_Index: e.detail.value,
    });
    this.data.hobby_chs[0] = this.data.hobby_item[this.data.hobby1_Index];
  },
  chsHby2: function (e) {
    if (e.detail.value == this.data.hobby2_Index) {
      return;
    }
    this.setData({
      hobby2_Index: e.detail.value,
    });
    this.data.hobby_chs[1] = this.data.hobby_item[this.data.hobby2_Index];
  },
  chsHby3: function (e) {
    if (e.detail.value == this.data.hobby3_Index) {
      return;
    }
    this.setData({
      hobby3_Index: e.detail.value,
    });
    this.data.hobby_chs[2] = this.data.hobby_item[this.data.hobby3_Index];
  },
  chsHby4: function (e) {
    if (e.detail.value == this.data.hobby4_Index) {
      return;
    }
    this.setData({
      hobby4_Index: e.detail.value,
    });
    this.data.hobby_chs[3] = this.data.hobby_item[this.data.hobby4_Index];
  },
  chsHby5: function (e) {
    if (e.detail.value == this.data.hobby5_Index) {
      return;
    }
    this.setData({
      hobby5_Index: e.detail.value,
    });
    this.data.hobby_chs[4] = this.data.hobby_item[this.data.hobby5_Index];
  },
  chsHby6: function (e) {
    if (e.detail.value == this.data.hobby6_Index) {
      return;
    }
    this.setData({
      hobby6_Index: e.detail.value,
    });
    this.data.hobby_chs[5] = this.data.hobby_item[this.data.hobby6_Index];
  },
  chgHby: function (res) {
    var that = this;
    getApp().request({
      url: '/user/hobby',
      method: 'POST',
      data: {
        open_id: getApp().globalData.userInfo.open_id,
        hobby: that.data.hobby_chs
      },
      success: function (res) {
        console.log(res);
        if(res.statusCode==200){
        getApp().globalData.userInfo.habit_1 = that.data.hobby_chs[0];
        getApp().globalData.userInfo.habit_2 = that.data.hobby_chs[1];
        getApp().globalData.userInfo.habit_3 = that.data.hobby_chs[2];
        getApp().globalData.userInfo.habit_4 = that.data.hobby_chs[3];
        getApp().globalData.userInfo.habit_5 = that.data.hobby_chs[4];
        getApp().globalData.userInfo.habit_6 = that.data.hobby_chs[5];
        wx.showToast({
          title: '修改成功',
          duration: 1500
        });
        }else{
          wx.showToast({
            title: '修改失败',
            duration: 1500,
            icon:'none'
          });
        }
      }
    });
    this.setData({
      hobby_set: !this.data.hobby_set
    });
  },
  closeModel2: function () {
    this.setData({
      hobby_set: !this.data.hobby_set
    });
  },
  register: function () {
    if (getApp().globalData.userInfo.user_group_id == 0) {
      wx.redirectTo({
        url: '../register/register-step1/register-step1',
      })
    }
  }
})