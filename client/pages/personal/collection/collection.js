var config = require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cor_item:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (option) {
    /*
    var that=this;
    wx.request({
      url:config.host+'/corFollowing',
      data:{
        open_id: getApp().globalData.userInfo.open_id
      },
      success:function(res){
        that.setData({
          cor_item:res.data
        });
        console.log(that.data.cor_item);
      }
    })
    */
  },
  onColletionTap: function (event) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    wx.request({
      url: config.host + '/corFollowing',
      data: {
        open_id: getApp().globalData.userInfo.open_id
      },
      success: function (res) {
        that.setData({
          cor_item: res.data
        });
        console.log(that.data.cor_item);
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
  cancel: function(res){
    console.log(res);
    console.log(this.data.cor_item[res.currentTarget.id]);
    var that=this;
    wx.request({
      url: config.host + '/corFollowing',
      method:'DELETE',
      data:{
        corDat:that.data.cor_item[res.currentTarget.id],
        open_id: getApp().globalData.userInfo.open_id
      },
      success:function(res){
        console.log(res);
      }
    });
    this.onShow();
  },
  toCor: function(){
    wx.navigateTo({
      url: '../corlist/corlist',
    })
  }
})