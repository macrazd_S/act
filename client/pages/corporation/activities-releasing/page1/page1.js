var config = require('../../../../config');


Page({

  /**
   * 页面的初始数据
   */
  data: {
    corInfoList: [],
    managerList: [],
    manager_array: [],
    manager_index: 0,
    coverImg: { errMsg: null, tempFilePaths: [], tmpFilees: [] },
    coverUrl: '',
    themeImg: {errMsg:null,tempFilePaths:[],tmpFilees:[]},
    themeUrl: '',
    date: '2016-11-08',
    times: '00:00',
    range_array: ['全校', '社团内部', '部门内部', '所有人'],
    class_array: [],
    classList:[],
    tag_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    range_index: 0,
    class_index: 0,
    tag1_index: 0,
    tag2_index: 0,
    tag3_index: 0,
    tag4_index: 0,
    tag5_index: 0,
    tag6_index: 0,
    actInfoList: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var defaultCoverTmp=this.data.coverImg;
    var defaultThemeTmp = this.data.themeImg;
    defaultCoverTmp.tempFilePaths[0]="http://tmp/wx9a46c33f1c97999f.o6zAJszydwS6_ek7C4I0hxxG9EIY.HjumqGofBQOP4633c4ae8c1a4abbd114e867ea4d5831.png";
    defaultThemeTmp.tempFilePaths[0] = "http://tmp/wx9a46c33f1c97999f.o6zAJszydwS6_ek7C4I0hxxG9EIY.HjumqGofBQOP4633c4ae8c1a4abbd114e867ea4d5831.png";
    this.setData({ 
      coverImg: defaultCoverTmp, 
      themeImg: defaultThemeTmp,
      coverUrl: defaultCoverTmp.tempFilePaths[0],
      themeUrl: defaultThemeTmp.tempFilePaths[0],
      })
    var that = this;
    var managerArr = [];
    var classArr=[];
    var i;
    console.log('获取到的缓存');
    wx.getStorage({
      key: 'corInfo',
      success: function (res) {
        console.log(res);
        for (i = 0; i < res.data.managerList.length; i++) {
          managerArr[i] = res.data.managerList[i].manager_name;
        }
        that.setData({
          corInfoList: res.data.corInfo,
          managerList: res.data.managerList,
          manager_array: managerArr,
        });
      },
    });
    wx.getStorage({
      key: 'class',
      success: function (res) {
        console.log(res);
        for (i = 0; i < res.data.length; i++) {
          classArr[i] = res.data[i].classification_name;
        }
        that.setData({ 
          classList: res.data ,
          class_array:classArr
          });
      },
    });
    wx.getStorage({
      key: 'actInfo',
      success: function (res) {
        console.log(res);
        that.setData({ actInfoList: res.data });
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  adminChg: function (e) {
    console.log(e.detail.value)
    this.setData({
      manager_index: e.detail.value
    })
  },
  bindDateChange: function (e) {
    console.log(e.detail.value)
    this.setData({
      dates: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    console.log(e.detail.value)
    this.setData({
      times: e.detail.value
    })
  },
  bindPickerChange1: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      range_index: e.detail.value
    })
  },
  bindPickerChange2: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      class_index: e.detail.value
    })
  },
  bindPickerChange3_1: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tag1_index: e.detail.value
    })
  },
  bindPickerChange3_2: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tag2_index: e.detail.value
    })
  },
  bindPickerChange3_3: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tag3_index: e.detail.value
    })
  },
  bindPickerChange3_4: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tag4_index: e.detail.value
    })
  },
  bindPickerChange3_5: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tag5_index: e.detail.value
    })
  },
  bindPickerChange3_6: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tag6_index: e.detail.value
    })
  },
  chooseimage1: function () {
    var that = this;
    wx.chooseImage({
      success: function (res) {
        that.setData({ coverImg: res });
        that.setData({ coverUrl: that.data.coverImg.tempFilePaths[0] });
      }
    });
  },
  chooseimage2: function () {
    var that = this;
    wx.chooseImage({
      success: function (res) {
        console.log('themeImg:');
        console.log(res);
        that.setData({ themeImg: res });
        that.setData({ themeUrl: that.data.themeImg.tempFilePaths[0] });
      }
    });
  },
  formSubmit: function (e) {
    console.log(e);
    this.data.actInfoList.activity_name = e.detail.value.activity_name;
    this.data.actInfoList.manager_open_id = this.data.managerList[this.data.manager_index].open_id;
    this.data.actInfoList.classification_id = this.data.classList[this.data.class_index].classification_id;
    this.data.actInfoList.manager_qq = e.detail.value.manager_qq;
    this.data.actInfoList.manager_tel = e.detail.value.manager_tel;
    this.data.actInfoList.manager_email = e.detail.value.manager_email;
    this.data.actInfoList.start_time = e.detail.value.activity_date + ' ' + e.detail.value.activity_time;
    this.data.actInfoList.end_time = null;
    this.data.actInfoList.activity_place = e.detail.value.activity_place;
    this.data.actInfoList.activity_range = e.detail.value.activity_range;
    this.data.actInfoList.passage_url = e.detail.value.passage_url;
    this.data.actInfoList.other_matter = e.detail.value.other_matter;
    this.data.actInfoList.appendix_url = null;
    this.data.actInfoList.activity_tag1 = e.detail.value.activity_tag1;
    this.data.actInfoList.activity_tag2 = e.detail.value.activity_tag2;
    this.data.actInfoList.activity_tag3 = e.detail.value.activity_tag3;
    this.data.actInfoList.activity_tag4 = e.detail.value.activity_tag4;
    this.data.actInfoList.activity_tag5 = e.detail.value.activity_tag5;
    this.data.actInfoList.activity_tag6 = e.detail.value.activity_tag6;
    console.log(this.data.actInfoList);
    var that = this;
    wx.uploadFile({
      header: {
        skey: wx.getStorageSync('skey')
      },
      url: config.host + '/corAdmin/uploadCover',
      filePath: that.data.coverImg.tempFilePaths[0],
      name: 'cover',
      success: function (res) {
        console.log(res);
        that.data.actInfoList.coverUrl = res.data;
        wx.uploadFile({
          header: {
            skey: wx.getStorageSync('skey')
          },
          url: config.host + '/corAdmin/uploadTheme',
          filePath: that.data.themeImg.tempFilePaths[0],
          name: 'theme',
          success: function (res) {
            console.log(res);
            that.data.actInfoList.themeUrl = res.data;
            that.setData({ themeUrl: res.data });
            wx.request({
              url: config.host + '/corAdmin/newAct',
              method: 'POST',
              data: that.data.actInfoList
            })
          }
        });
      }
    });
  }
})