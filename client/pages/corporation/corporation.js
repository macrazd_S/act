
var config = require('../../config');
Page({
  data: {
    corList: [],
    nowCor: null,
    corIndex: 0,
    coverImg: [],
    themeImg: [],
    corInfoList: [],
    corChsInfo: [],
    actInfoList: {
      activity_name: null,
      community_id: null,
      manager_open_id: null,
      classification_id: null,
      sch_id: null,  //actList中有,从上一界面传过来
      manager_qq: null,
      manager_tel: null,
      manager_email: null,
      start_time: null,
      end_time: null,
      activity_place: null,
      activity_range: null,
      coverUrl: null,
      themeUrl: null,
      passage_url: null,
      other_matter: null,
      appendix_url: null,
      activity_tag1: null,
      activity_tag2: null,
      activity_tag3: null,
      activity_tag4: null,
      activity_tag5: null,
      activity_tag6: null,
      sta_id:1,
    },
    myPos:'',
    allCorAdmin:false,
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var result = [];
    wx.request({
      url: config.host + '/corAdmin',
      data: { open_id: getApp().globalData.userInfo.open_id },
      success: function (res) {
        if(res.data.length==0){
          wx.showToast({
            title: '您暂未关注社团',
            icon: 'none',
            duration: 2000
          })
          return;
        }
        console.log(res.data);
        var i;
        for (i = 0; i < res.data.length; i++) {
          result[i] = res.data[i].community_name;
        }
        that.setData({
          myPos: res.data[that.data.corIndex].user_pos,
          corInfoList: res.data,
          corList: result,
        });
        wx.setStorage({
          key: 'myPos',
          data: that.data.myPos,
        });
        wx.setStorage({
          key: 'corInfoList',
          data: that.data.corInfoList,
        });
        wx.request({
          url: config.host + '/corAdmin/infoList',
          data: { community_id: that.data.corInfoList[0].community_id },
          success: function (res) {
            console.log(res);
            /*
            for(i=0;i<res.data.manager.length;i++){
              if(myPos!='会长'&&res.data.manager[i].open_id==getApp().globalData.userInfo.open_id){
                  that.setData({myPos:res.data.manager[i].position});
              }
            }
            */
            wx.request({
              url: config.host+'/corAdmin/allCorAdmin',
              data:{
                open_id:getApp().globalData.userInfo.open_id,
                position:'社团管理员'
              },
              success:function(res){
                if(res.data.length>0){
                  that.setData({ allCorAdmin: true });
                }
              }
            })
            wx.setStorage({
              key: 'class',
              data: res.data.classific,
            })
            var corInfo = {
              corInfo: {
                sch_id: that.data.corInfoList[that.data.corIndex].sch_id,
                community_id: that.data.corInfoList[that.data.corIndex].community_id,
                community_name: that.data.corInfoList[that.data.corIndex].community_name
              },
              managerList: res.data.manager,
            }
            wx.setStorage({
              key: 'corInfo',
              data: corInfo,
            })
            that.data.actInfoList.community_id = that.data.corInfoList[that.data.corIndex].community_id;
            that.data.actInfoList.sch_id = that.data.corInfoList[that.data.corIndex].sch_id;
            wx.setStorage({
              key: 'actInfo',
              data: that.data.actInfoList,
            })
          }
        });
      }
    });
  },

  onShareAppMessage: function () {

  },
  chsCor: function (res) {
    console.log(res);
    var that = this;
    this.setData({ corIndex: res.detail.value });
    this.setData({ myPos: that.data.corInfoList[that.data.corIndex].user_pos});
    wx.setStorage({
      key: 'myPos',
      data: that.data.myPos,
    });
    wx.request({
      url: config.host + '/corAdmin/infoList',
      data: { community_id: that.data.corInfoList[that.data.corIndex].community_id },
      success: function (res) {
        that.data.actInfoList.community_id = that.data.corInfoList[that.data.corIndex].community_id;
        that.data.actInfoList.sch_id = that.data.corInfoList[that.data.corIndex].sch_id;
        wx.setStorage({
          key: 'actInfo',
          data: that.data.actInfoList,
        });
        var corInfo = {
          corInfo: {
            sch_id: that.data.corInfoList[that.data.corIndex].sch_id,
            community_id: that.data.corInfoList[that.data.corIndex].community_id,
            community_name: that.data.corInfoList[that.data.corIndex].community_name
          },
          managerList: res.data.manager,
        }
        wx.setStorage({
          key: 'corInfo',
          data: corInfo,
        })
      }
    });
  },
  topage1: function () {
    wx.navigateTo({
      url: './activities-releasing/page1/page1',
    })
  },
  toproperties: function () {
    wx.navigateTo({
      url: './properties/properties',
    })
  },
  toact_setting: function () {
    wx.navigateTo({
      url: './activities-setting/activities-setting',
    })
  },
  toSuggestion:function(){
    wx.navigateTo({
      url: './suggestion/suggestion',
    })
  },
  toAdminManage: function () {
    wx.navigateTo({
      url: './admin-setting/admin-setting',
    })
  },
  toCorManage: function () {
    wx.navigateTo({
      url: './corManagement/corManagement',
    })
  },
  toAdminApp: function () {
    wx.navigateTo({
      url: './adminApplication/adminApplication',
    })
  },
})