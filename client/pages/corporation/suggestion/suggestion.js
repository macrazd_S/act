var config = require('../../../config');
Page({
  data: {
    actList: [],
    actArr: [],
    actIndex: 0,
    community_id: null,
    actInfoList: {},
    myPos: '',
  },
  onLoad: function () {
    var that = this;
    var result = [];
    var actArrTmp = [];
    var i;
    wx.getStorage({
      key: 'myPos',
      success: function (res) {
        console.log(res);
        that.setData({ myPos: res.data });
      },
    })
    wx.getStorage({
      key: 'corInfo',
      success: function (res) {
        that.setData({ community_id: res.data.corInfo.community_id });
        wx.request({
          url: config.host + '/corAdmin/actList',
          data: { community_id: that.data.community_id },
          success: function (ret) {
            var actListT = ret.data;
            for (i = 0; i < ret.data.length; i++) {
              var state = '';
              if (ret.data[i].sta_id == 0) {
                actArrTmp[i] = ret.data[i].activity_name + '(归档)';
              } else if (ret.data[i].sta_id == 1) {
                actArrTmp[i] = ret.data[i].activity_name + '(发布中)';
              } else {
                actArrTmp[i] = ret.data[i].activity_name + '(已过期)';
              }
            }
            that.setData({
              actArr: actArrTmp,
              actList: actListT
            });
            //console.log(that.data.actList[that.data.actIndex]);
            wx.setStorage({
              key: 'actInfo',
              data: that.data.actList[that.data.actIndex],
            });
          }
        })
      },
    })
  },
  sug_forexi: function () {
    wx.navigateTo({
      url: './existent/existent',
    })
  },
  sug_fornew: function () {
    wx.navigateTo({
      url: './non-existent/non-existent',
    })
  },
  sug_list: function () {
    wx.navigateTo({
      url: './suggestionList/suggestionList',
    })
  },
  sug_my: function () {
    wx.navigateTo({
      url: './mySuggestions/mySuggestions',
    })
  }




})