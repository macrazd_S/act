var config=require('../../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sug_list: [],
    detailFlag:true,
    content:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    wx.getStorage({
      key: 'actInfo',
      success: function (res) {
        console.log(res.data);
        wx.request({
          url: config.host+'/corAdmin/getSug',
          data:{activity_id:res.data.activity_id},
          success:function(ret){
            console.log(ret);
            //设置值  添加点击查看详情
            that.setData({sug_list:ret.data})
          }
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  sugDetail:function(e){
    console.log(e);
    this.setData({
      detailFlag: !this.data.detailFlag,
      content:this.data.sug_list[e.target.id].content
      });
  },
  closeModel:function(){
    this.setData({ 
      detailFlag: !this.data.detailFlag 
    });
  }
})