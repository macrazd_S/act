var config=require('../../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appList:[]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    wx.getStorage({
      key:'corInfoList',
      success: function(res) {
        console.log(res);
        that.setData({appList:res.data})
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  delCor:function(e){
    /*
    //接口缺少删除涉及该社团的活动及其他相关表的数据项
    var that=this;
    var id=e.target.id;
    console.log(e);
    wx.request({
      url: config.host+'/corAdmin/delCor',
      method:'DELETE',
      data:{community_id:that.data.appList[id].community_id},
      success:function(res){
        console.log()
        console.log(res);
      }
    })
    */
  }
})