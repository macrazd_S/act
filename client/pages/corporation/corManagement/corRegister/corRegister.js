var config=require('../../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    corInfo:{
      community_name:'',
      president_open_id:'',
      sch_id:null,
      tag_1:'',
      tag_2: '',
      tag_3: '',
      tag_4: '',
      tag_5: '',
      tag_6: '',
      community_avatar:null
    },
    tag_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag1_index:0,
    tag2_index: 0,
    tag3_index: 0,
    tag4_index: 0,
    tag5_index: 0,
    tag6_index: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindPickerChange3_1: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag1_index: e.detail.value,
    });
  },
  bindPickerChange3_2: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag2_index: e.detail.value
    });
  },
  bindPickerChange3_3: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag3_index: e.detail.value
    });
  },
  bindPickerChange3_4: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag4_index: e.detail.value
    });
  },
  bindPickerChange3_5: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag5_index: e.detail.value
    });
  },
  bindPickerChange3_6: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag6_index: e.detail.value
    });
  },
  formSubmit:function(e){
    console.log(e);
    var that=this;
    this.data.corInfo={
      community_name: e.detail.value.activity_name,
      president_open_id:getApp().globalData.userInfo.open_id,
      sch_id: getApp().globalData.userInfo.sch_id,
      tag_1: that.data.tag_array[e.detail.value.activity_tag1],
      tag_2: that.data.tag_array[e.detail.value.activity_tag2],
      tag_3: that.data.tag_array[e.detail.value.activity_tag3],
      tag_4: that.data.tag_array[e.detail.value.activity_tag4],
      tag_5: that.data.tag_array[e.detail.value.activity_tag5],
      tag_6: that.data.tag_array[e.detail.value.activity_tag6],
      community_avatar: null
    };
    console.log(this.data.corInfo);
    wx.request({
      url: config.host+'/corAdmin/newCorApp',
      method:'POST',
      data:that.data.corInfo,
      success:function(res){
        console.log(res);
      }
    })
  }

})