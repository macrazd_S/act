var config=require('../../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appList: [],
    pre_name:[],
    subInfo:{
      community_id:null,
      community_name:'',
      president_open_id:'',
      sch_id:null,
      community_avatar:'',
      tag_1:'',
      tag_2: '',
      tag_3:'',
      tag_4: '',
      tag_5: '',
      tag_6: '',
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    wx.request({
      url: config.host+'/corAdmin/getCorApp',
      data:{sch_id:getApp().globalData.userInfo.sch_id},
      success:function(res){
        console.log(res);
        that.setData({
          appList:res.data.corList,
          pre_name:res.data.president
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  accept:function(e){
    //删除数组中的该项
    console.log(e);
    var that = this;
    var id = e.target.id;
    console.log(that.data.appList);
    this.data.subInfo={
      community_id: null,
      community_name: that.data.appList[id].community_name,
      president_open_id: that.data.appList[id].president_open_id,
      sch_id: that.data.appList[id].sch_id,
      community_avatar: that.data.appList[id].community_avatar,
      tag_1: that.data.appList[id].tag_1,
      tag_2: that.data.appList[id].tag_2,
      tag_3: that.data.appList[id].tag_3,
      tag_4: that.data.appList[id].tag_4,
      tag_5: that.data.appList[id].tag_5,
      tag_6: that.data.appList[id].tag_6,
    }
    wx.request({
      url: config.host+'/corAdmin/acCorApp',
      method:'POST',
      data:that.data.subInfo,
      success:function(res){
        console.log('已通过');
        console.log(res);
        wx.request({
          url: config.host + '/corAdmin/rjCorApp',
          method: 'DELETE',
          data: that.data.appList[id],
          success: function (res) {
            console.log('已删除该申请');
            console.log(res);
            var tmp = that.data.appList;
            tmp.splice(id, 1);
            that.setData({ appList: tmp });
          }
        })
      }
    })
  },
  reject:function(e){
    var that = this;
    var id = e.target.id;
    wx.request({
      url: config.host+'/corAdmin/rjCorApp',
      method:'DELETE',
      data: that.data.appList[id],
      success:function(res){
        console.log('已拒绝');
        console.log(res);
        var tmp = that.data.appList;
        tmp.splice(id, 1);
        that.setData({ appList: tmp });
      }
    })
  }
})