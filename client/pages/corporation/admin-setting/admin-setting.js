var config = require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appList: [],
    delFlag: true,
    posFlag: true,
    posIndex: 0,
    posList: ['部长', '副会长'],
    chsPos: '',
    open_id: '',
    community_id: null,
    chsAdmin: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'corInfo',
      success: function (res) {
        console.log(res);
        that.setData({ community_id: res.data.corInfo.community_id });
        wx.request({
          url: config.host + '/corAdmin/getAdminList',
          data: { community_id: res.data.corInfo.community_id },
          success: function (ret) {
            console.log(ret);
            that.setData({ appList: ret.data });
          }
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //显示职位修改modal
  chgPos: function (e) {
    //console.log(e);
    if (this.data.appList[e.target.id].position == '部长') {
      this.setData({ chsPos: '部长', posIndex: 0 });
    } else {
      this.setData({ chsPos: '副会长', posIndex: 1 });
    }
    this.setData({
      chsAdmin: e.target.id,
      open_id: this.data.appList[e.target.id].open_id,
      posFlag: !this.data.posFlag
    });
    console.log(this.data);
  },
  //选择职位
  chsPos: function (e) {
    //console.log(e);
    this.setData({ posIndex: e.detail.value });
    if (this.data.chsPos != this.data.posList[e.detail.value]) {
      this.setData({ chsPos: this.data.posList[e.detail.value] });
    }
  },
  //确认修改职位
  confirmModel1: function (e) {
    var that = this;
    console.log(e);
    var chgInfo = {
      open_id: this.data.open_id,
      community_id: this.data.community_id,
      position: this.data.chsPos
    };
    console.log(chgInfo);
    wx.request({
      url: config.host + '/corAdmin/chgPos',
      method: 'POST',
      data: chgInfo,
      success: function (res) {
        console.log('修改成功');
      }
    });
    var tmp = that.data.appList;
    tmp[that.data.chsAdmin].position = that.data.chsPos;
    this.setData({ appList: tmp, posFlag: !this.data.posFlag });
  },
  //取消修改职位
  cancelModel: function () {
    this.setData({ posFlag: !this.data.posFlag });
  },
  //显示移除管理员modal
  delAdmin: function (e) {
    console.log(e);
    this.setData({
      chsAdmin: e.target.id,
      open_id: this.data.appList[e.target.id].open_id,
      delFlag: !this.data.delFlag
    });
  },
  //确认删除管理员
  confirmModel2: function () {
    var that=this;
    wx.request({
      url: config.host+'/corAdmin/delAdmin',
      method:'DELETE',
      data:{
        open_id:that.data.open_id,
        community_id:that.data.community_id
      },
      success:function(res){
        console.log('删除成功');
        console.log(res);
        var tmp = that.data.appList;
        tmp.splice(that.data.chsAdmin, 1);
        that.setData({appList: tmp});
      }
    });
    this.setData({ delFlag: !this.data.delFlag });
  },
  //取消修改职位
  cancelModel2: function () {
    this.setData({ delFlag: !this.data.delFlag });
  },
})