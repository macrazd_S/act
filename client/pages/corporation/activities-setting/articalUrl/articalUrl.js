var config = require('../../../../config.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    coverImg: null,
    coverUrl: '',
    themeImg: null,
    themeUrl: '',
    other_matter: '',
    passage_url: '',
    actInfoList:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'actInfo',
      success: function (res) {
        console.log(res);
        that.setData({
          themeUrl: res.data.theme_url,
          coverUrl: res.data.cover_url,
          other_matter: res.data.other_matter,
          passage_url: res.data.passage_url,
          actInfoList:res.data
        });
      },
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  chooseimage1: function () {
    var that = this;
    wx.chooseImage({
      success: function (res) {
        that.setData({ coverImg: res });
        that.setData({ coverUrl: that.data.coverImg.tempFilePaths[0] });
      }
    });
  },
  chooseimage2: function () {
    var that = this;
    wx.chooseImage({
      success: function (res) {
        that.setData({ themeImg: res });
        that.setData({ themeUrl: that.data.themeImg.tempFilePaths[0] });
      }
    });
  },
  formSubmit: function (e) {
    console.log('form发生了submit事件，携带数据为：');
    console.log(e);
    console.log(this.data.actInfoList);
    this.data.actInfoList.passage_url = e.detail.value.passage_url;
    this.data.actInfoList.other_matter = e.detail.value.otherthing;
    console.log(this.data.actInfoList);
    var that = this;
    if(that.data.coverImg){
      wx.uploadFile({
        header: {
          skey: wx.getStorageSync('skey')
        },
        url: config.host + '/corAdmin/uploadCover',
        filePath: that.data.coverImg.tempFilePaths[0],
        name: 'cover',
        success: function (res) {
          console.log(res);
          that.data.actInfoList.coverUrl = res.data;

        }
      });
    }
    if(that.data.themeImg){
      wx.uploadFile({
        header: {
          skey: wx.getStorageSync('skey')
        },
        url: config.host + '/corAdmin/uploadTheme',
        filePath: that.data.themeImg.tempFilePaths[0],
        name: 'theme',
        success: function (res) {
          console.log(res);
          that.data.actInfoList.themeUrl = res.data;
        }
      });
    }
    wx.request({
      url: config.host + '/corAdmin/updateAct',
      method: 'POST',
      data: that.data.actInfoList
    })
  },
})