var config = require('../../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    state: '发布',
    sta_id: 1,
    actInfoList:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getStorage({
      key: 'actInfo',
      success: function (res) {
        console.log(res);
        that.setData({actInfoList:res.data});
        if (res.data.sta_id == 0) {
          that.setData({ state: '归档', sta_id: 0 });
        } else if (res.data.sta_id == 1) {
          that.setData({ state: '发布', sta_id: 1 });
        } else {
          that.setData({ state: '已过期', sta_id: 2 });
        }
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  setgd: function () {
    var that=this;
    if (this.data.sta_id == 2) return;
    else {
      this.setData({
        state: '归档'
      });
      this.data.actInfoList.sta_id=0;
      wx.request({
        url: config.host + '/corAdmin/updateAct',
        method: 'POST',
        data: that.data.actInfoList
      })
    }
  },
  setfb: function () {
    var that=this;
    if (this.data.sta_id == 2) return;
    else {
      this.setData({
        state: '发布'
      });
      this.data.actInfoList.sta_id = 1;
      wx.request({
        url: config.host + '/corAdmin/updateAct',
        method: 'POST',
        data: that.data.actInfoList
      })
    }
  }
})