var config = require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    actList: [],
    actArr: [],
    actIndex: 0,
    community_id: null,
    actInfoList: {},
    delFlag:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('已发布活动管理：');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var result = [];
    var actArrTmp = [];
    var i;
    wx.getStorage({
      key: 'corInfo',
      success: function (res) {
        that.setData({ community_id: res.data.corInfo.community_id });
        wx.request({
          url: config.host + '/corAdmin/actList',
          data: { community_id: that.data.community_id },
          success: function (ret) {
            var actListT = ret.data;
            for (i = 0; i < ret.data.length; i++) {
              var state = '';
              if (ret.data[i].sta_id == 0) {
                actArrTmp[i] = ret.data[i].activity_name + '(归档)';
              } else if (ret.data[i].sta_id == 1) {
                actArrTmp[i] = ret.data[i].activity_name + '(发布中)';
              } else {
                actArrTmp[i] = ret.data[i].activity_name + '(已过期)';
              }
            }
            that.setData({
              actArr: actArrTmp,
              actList: actListT
            });
            wx.setStorage({
              key: 'actInfo',
              data: that.data.actList[that.data.actIndex],
            });
          }
        })
      },
    })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindPickerChange: function (e) {
    var that=this;
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      actIndex: e.detail.value
    });
    wx.setStorage({
      key: 'actInfo',
      data: that.data.actList[that.data.actIndex],
    })
    console.log(that.data.actList[that.data.actIndex].activity_id);
  },
  delConfirm:function(){
    this.setData({delFlag:!this.data.delFlag});
  },
  confirmModel:function(){
    var that=this;
    wx.request({
      url: config.host+'/corAdmin/delAct',
      method:'DELETE',
      data:{
        activity_id: that.data.actList[that.data.actIndex].activity_id
      },
      success:function(res){
        console.log('删除成功！');
        console.log(res);
      }
    })
    this.setData({ delFlag: !this.data.delFlag });
  },
})