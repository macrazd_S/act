var config=require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appList: [],
    name:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    wx.getStorage({
      key: 'corInfo',
      success: function(res) {
        //console.log(res);
        wx.request({
          url: config.host + '/corAdmin/getAdminApp',
          data:{community_id:res.data.corInfo.community_id},
          success:function(ret){
            console.log(ret);
            var appTmp = [];
            var namTmp=[];
            var i,j;
            for(i=0,j=0;i<ret.data.appList.length;i++){
              if (ret.data.appList[i].state=='审核中'){
                appTmp[j]=ret.data.appList[i];
                namTmp[j] = ret.data.name[i];
                j++;
              }
            }
            that.setData({
              appList:appTmp,
              name:namTmp
            })
          }
        })
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  accept:function(e){
    var that=this;
    console.log(e);
    wx.request({
      url: config.host +'/corAdmin/acAdminApp',
      method:'POST',
      data:{
        open_id:that.data.appList[e.target.id].open_id,
        position: that.data.appList[e.target.id].position,
        community_id: that.data.appList[e.target.id].community_id
      },
      success:function(res){
        var tmp = that.data.appList;
        tmp.splice(e.target.id, 1);
        that.setData({ appList: tmp });
        console.log(res);
      }
    })
  },
  reject:function(e){
    var that = this;
    console.log(e);
    wx.request({
      url: config.host + '/corAdmin/rjAdminApp',
      method: 'POST',
      data: {
        open_id: that.data.appList[e.target.id].open_id,
        position: that.data.appList[e.target.id].position,
        community_id: that.data.appList[e.target.id].community_id
      },
      success: function (res) {
        var tmp = that.data.appList;
        tmp.splice(e.target.id, 1);
        that.setData({ appList: tmp });
        console.log(res);
      }
    })
  }
})