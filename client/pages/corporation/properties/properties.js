var config = require('../../../config');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    corNam: null,
    tag1_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag2_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag3_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag4_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag5_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag6_array: ['无', '体育', '音乐', '阅读', '文艺', '娱乐'],
    tag1_index: 0,
    tag2_index: 0,
    tag3_index: 0,
    tag4_index: 0,
    tag5_index: 0,
    tag6_index: 0,
    corInfo: {},
    updateInfo: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this;
    var i;
    wx.getStorage({
      key: 'corInfo',
      success: function (res) {
        console.log(res);
        that.setData({ corInfo: res.data.corInfo });
        //console.log(that.data.corInfo);
        wx.request({
          url: config.host + '/corAdmin/getCorTag',
          data: { community_id: that.data.corInfo.community_id },
          success: function (ret) {
            var res=ret;
            console.log(ret);
            that.setData({
              corNam: ret.data[0].community_name,
              updateInfo: ret.data[0],
            });
            for (i = 0; i < 6; i++) {
              if (ret.data[0].tag_1 == null) {
                that.setData({ tag1_index: 0 });
                break;
              } else if (res.data[0].tag_1 == that.data.tag1_array[i]) {
                that.setData({ tag1_index: i });
                break;
              }
            }
            for (i = 0; i < 6; i++) {
              if (ret.data[0].tag_2 == null) {
                that.setData({ tag2_index: 0 });
                break;
              } else if (res.data[0].tag_2 == that.data.tag2_array[i]) {
                that.setData({ tag2_index: i });
                break;
              }
            }
            for (i = 0; i < 6; i++) {
              if (ret.data[0].tag_3 == null) {
                that.setData({ tag3_index: 0 });
                break;
              } else if (res.data[0].tag_3 == that.data.tag3_array[i]) {
                that.setData({ tag3_index: i });
                break;
              }
            }
            for (i = 0; i < 6; i++) {
              if (ret.data[0].tag_4 == null) {
                that.setData({ tag4_index: 0 });
                break;
              } else if (res.data[0].tag_4 == that.data.tag4_array[i]) {
                that.setData({ tag4_index: i });
                break;
              }
            }
            for (i = 0; i < 6; i++) {
              if (ret.data[0].tag_5 == null) {
                that.setData({ tag5_index: 0 });
                break;
              } else if (res.data[0].tag_5 == that.data.tag5_array[i]) {
                that.setData({ tag5_index: i });
                break;
              }
            }
            for (i = 0; i < 6; i++) {
              if (ret.data[0].tag_6 == null) {
                that.setData({ tag6_index: 0 });
                break;
              } else if (res.data[0].tag_6 == that.data.tag6_array[i]) {
                that.setData({ tag6_index: i });
                break;
              }
            }
          }
        })
      },
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  setName: function (e) {
    console.log(e);
    this.data.updateInfo.community_name = e.detail.value;
  },
  bindPickerChange3_1: function (e) {
    var that=this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag1_index: e.detail.value,
    });
    that.data.updateInfo.tag_1 = that.data.tag1_array[this.data.tag1_index];
  },
  bindPickerChange3_2: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag2_index: e.detail.value
    });
    that.data.updateInfo.tag_2 = that.data.tag2_array[this.data.tag2_index];
  },
  bindPickerChange3_3: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag3_index: e.detail.value
    });
    that.data.updateInfo.tag_3 = that.data.tag3_array[this.data.tag3_index];
  },
  bindPickerChange3_4: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag4_index: e.detail.value
    });
    that.data.updateInfo.tag_4 = that.data.tag4_array[this.data.tag4_index];
  },
  bindPickerChange3_5: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag5_index: e.detail.value
    });
    that.data.updateInfo.tag_5 = that.data.tag5_array[this.data.tag5_index];
  },
  bindPickerChange3_6: function (e) {
    var that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      tag6_index: e.detail.value
    });
    that.data.updateInfo.tag_6 = that.data.tag6_array[this.data.tag6_index];
  },

  formSubmit: function (e) {
    var that=this;
    console.log('form发生了submit事件，携带数据为：', e.detail.value);
    wx.request({
      url: config.host+'/corAdmin/updateCorTag',
      method:'POST',
      data:that.data.updateInfo,
      success:function(res){
        console.log(res);
      }
    })
  },
})