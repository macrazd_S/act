Page({
  data: {
    schArr: [],
    arrIndex: 0,
    provincial: null,
    provinceItem: [],
    proIndex: 0,
    city: null,
    cityItem: [],
    cityIndex: 0,
    showDialog: false,
    showDialogRight: false,
    items: [
      { name: '0', value: '体育', checked: false, },
      { name: '1', value: '文艺', checked: false, },
      { name: '2', value: '展会', checked: false, },
      { name: '3', value: '市场', checked: false, },
      { name: '4', value: '讲座', checked: false, },
      { name: '5', value: '户外', checked: false, },
      { name: '6', value: '公益', checked: false, },
      { name: '7', value: '娱乐', checked: false, },
      { name: '8', value: '比赛', checked: false, },
      { name: '9', value: '表演', checked: false, },
      { name: '10', value: '科技', checked: false, },
      { name: '11', value: '美食', checked: false, },
    ],
    hobbyNum: 0,
    school: null,
    state: 0
  },
  onLoad: function (options) {
    wx.showLoading({
      title: '加载学校信息中...',
    });
    setTimeout(function () {
      wx.hideLoading();
    }, 800);
    this.setData({ provinceItem: getApp().globalData.province });
    this.data.province = this.data.provinceItem[0];
    var that = this;
    getApp().reqCity({
      data: {
        province: that.data.provinceItem[0]
      },
      success: function (res) {
        that.setData({ cityItem: res.data });
        that.data.city = that.data.cityItem[0];
        var th = that;
        getApp().reqSch({
          data: {
            province: th.data.province,
            city: th.data.city
          },
          success: function (ret) {
            th.setData({ schArr: ret.data });
            th.data.school = th.data.schArr[0];
          }
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '获取市列表失败',
          duration: 3000
        })
      }
    });
    //添加定位信息，设置provincial，city以及school
    /*
    DELETE FROM `user_info` WHERE `open_id`='o_aLD5KP2CEPssxstXic_cH7MZiE'
    //域名暂未备案，公众平台无法添加request合法域名，暂时无法逆解析经纬度
    //因此暂时需要手动选择地址
    var that=this;
    wx.getLocation({
      success: function(res) {
        console.log(res);
        var latitude = res.latitude;
        var longitude = res.longitude;
        wx.request({
          url: 'http://apis.map.qq.com/ws/geocoder/v1/',
          data:{
            location:latitude+','+longitude,
            key: 'IBZBZ-357KF-573JK-NDAVS-SSKTZ-K6BT7',
            get_poi:0
          },
          success:function(res){
            console.log(res);
            that.setData({
              provincial: res.result.address_component.province,
              city: res.result.address_component.city
            });
          }
        })
      },
    })
    */
  },
  onReady: function () {

  },
  onShow: function () {
    var that = this;
    wx.getStorage({
      key: 'location',
      success: function (res) {
        that.setData({
          provincial: res.data.provincial,
          city: res.data.city
        });
      }
    })
  },

  onHide: function () {

  },

  onUnload: function () {

  },

  onPullDownRefresh: function () {

  },

  onReachBottom: function () {

  },

  onShareAppMessage: function () {

  },
  nextstep: function () {
    var that=this;
    wx.setStorage({
      key: 'hobby',
      data: that.data.items,
    });
    wx.navigateTo({
      url: '../register-step2/register-step2?&school=' + this.data.school +'&hobbyNum=' + this.data.hobbyNum,
    });
  },
  noRegister: function () {
    getApp().registerUser(function () {
      wx.reLaunch({
        url: '../../index/index',
      })
    });
  },
  chsProvince: function (e) {
    if (e.detail.value == this.data.proIndex) {
      return;
    }
    var that = this;
    this.setData({
      proIndex: e.detail.value,
    });
    this.data.province = this.data.provinceItem[this.data.proIndex];
    getApp().reqCity({
      data: {
        province: that.data.province
      },
      success: function (res) {
        that.setData({ cityItem: res.data });
        that.data.city = that.data.cityItem[0];
        var th = that;
        getApp().reqSch({
          data: {
            province: th.data.province,
            city: th.data.city
          },
          success: function (ret) {
            th.setData({ schArr: ret.data });
            th.data.school = th.data.schArr[0];
          }
        });
      },
      fail: function (res) {
        wx.showToast({
          title: '获取市列表失败',
          duration: 3000
        })
      }
    });
  },
  chsCity: function (e) {
    if (e.detail.value == this.data.cityIndex) {
      return;
    }
    var that = this;
    this.setData({
      cityIndex: e.detail.value,
    });
    this.data.city = this.data.cityItem[this.data.cityIndex];
    getApp().reqSch({
      data: {
        province: that.data.province,
        city: that.data.city
      },
      success: function (ret) {
        console.log(ret);
        that.setData({ schArr: ret.data });
        that.data.school = that.data.schArr[0];
      },
      fail: function (res) {
        wx.showToast({
          title: '获取学校列表失败',
          duration: 3000
        })
      }
    });
  },
  chsSchool: function (e) {
    if (e.detail.value == this.data.arrIndex) {
      return;
    }
    this.setData({
      arrIndex: e.detail.value,
    });
    this.data.school = this.data.schArr[this.data.arrIndex];
  },
  touchStart: function (e) {
    this.setData({
      color: 'red',
      border: '1px solid red'
    })
    console.log(e)
  },
  checkChange: function (e) {
    console.log('radio发生change事件，携带value值为：', e)
    var that = this;
    var i = 0;
    for(i=0;i<12;i++){
      if (this.data.items[i].value==e.detail.value[e.detail.value.length - 1]) {
        break;
      }
    }
    var index=i-1;
    if (e.detail.value.length > 6 && !this.data.items[index].checked) {
      var tmp = [];
      for (i = 0; i < 6; i++) {
        tmp[i] = e.detail.value[i];
      }
      e.detail.value = tmp;
    } else {
      that.setData({
        value: e.detail.value
      })
      var items = this.data.items;
      var checkArr = e.detail.value;
      for (var i = 0; i < items.length; i++) {
        if (checkArr.indexOf(i + "") != -1) {
          items[i].checked = true;
        } else {
          items[i].checked = false;
        }
      }
      this.setData({
        hobbyNum:e.detail.value.length,
        items: items,
      })
    }
  },
})