Page({
  data: {
    school: null,
    hobby: [],
    hobbyNum:0
  },

  /**
   * DELETE FROM `user_info` WHERE `open_id`='o_aLD5KP2CEPssxstXic_cH7MZiE'
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    var i = 0;
    var j=0;
    var that=this;
    this.setData({ school: options.school,hobbyNum:options.hobbyNum });
    wx.getStorage({
      key: 'hobby',
      success: function(res) {
        console.log(res);
        for (i = 0; i < 12; i++) {
          if(res.data[i].checked){
            that.data.hobby[j]=res.data[i].value;
            j++;
          }
        }
        console.log(that.data);
      },
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  student: function () {
    getApp().globalData.userInfo.user_group_id = 1;
    getApp().globalData.userInfo.school_name = this.data.school;
    getApp().globalData.userInfo.habit_1 = this.data.hobby[0];
    getApp().globalData.userInfo.habit_2 = this.data.hobby[1];
    getApp().globalData.userInfo.habit_3 = this.data.hobby[2];
    getApp().globalData.userInfo.habit_4 = this.data.hobby[3];
    getApp().globalData.userInfo.habit_5 = this.data.hobby[4];
    getApp().globalData.userInfo.habit_6 = this.data.hobby[5];
    console.log(getApp().globalData.userInfo);
    getApp().registerUser(function () {
      wx.reLaunch({
        url: '../../index/index',
      })
    })
  },
  friend: function () {
    getApp().globalData.userInfo.user_group_id = 2;
    getApp().globalData.userInfo.school_name = this.data.school;
    getApp().registerUser(function () {
      wx.reLaunch({
        url: '../../index/index',
      })
    });
  }
})