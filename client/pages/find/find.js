var config = require('../../config')
Page({
   data:{
     array: ['北京理工大学', '北京师范大学', '吉林大学', '清华大学'],
     objectArray: [
       {
         id: 0,
         name: '北京理工大学'
       },
       {
         id: 1,
         name: '北京师范大学'
       },
       {
         id: 2,
         name: '吉林大学'
       },
       {
         id: 3,
         name: '清华大学'
       }
     ],
     index: 0,
     actList: [],

    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */  
  todetail: function (event) {
    var articleId = event.currentTarget.dataset.articleId
    console.log(articleId);

    wx.navigateTo({
      url: "../article/article?id=" + articleId,
    })
  },
  onShareAppMessage: function () {
    
  },
    bindPickerChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },

  showactivity: function (event) {
    var key = event.detail.value;
    var that = this;
    wx.request({
      url: config.host+'/search',
      data: {
         key: key,
         open_id:getApp().globalData.userInfo.open_id
       },
      success: function (res) {
           that.setData({
             actList: res.data
           })
      }
    });
    }

})